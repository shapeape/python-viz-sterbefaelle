#!/usr/bin/python3

"""
Compare age groups
"""

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import cm
from openpyxl import load_workbook

file = r'data/sonderauswertung-sterbefaelle.xlsx'

wb = load_workbook(file, data_only=True)
sheet = wb['D_2016-2020_Monate_AG_Ins']

months = ['Januar', 'Februar', 'März', 'April', 'Mai', 'Juni',
          'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']

# empty df to join later
df = pd.DataFrame()

# for final plot
all_dates = []

# get age groups
ages = []
for age_row in range(11, 27):
    age = sheet.cell(column=3, row=age_row).value
    ages.append(age.replace(' u. mehr', '+'))

# start in 2016, move towards 2020
for y in range(4, -1, -1):
    # all age groups per year
    deaths_ages = []
    # dates per year
    date = []

    # get current year
    year = sheet.cell(column=2, row=17 * y + 10).value

    # create dates for column names
    for m in months:
        date.append(m[0:3] + ' ' + str(year))
        all_dates.append(m[0:3] + ' ' + str(year))

    for age_row in range(11, 27):
        # new row for each age group
        row = []
        for col in range(4, 16):
            value = sheet.cell(column=col, row=17 * y + age_row).value
            row.append(value)
        deaths_ages.append(row)

    # dataframe for current year
    df_year = pd.DataFrame(deaths_ages, columns=date, index=ages)

    if y == 4:
        df = df_year
    else:
        df = pd.concat([df, df_year], axis=1, ignore_index=True)

# all dates as column names
df.columns = all_dates

# transpose dataframe
df = df.T

# try one of these: plasma_r, cubehelix_r, gnuplot2_r, CMRmap_r, brg, brg_r
gradient = cm.get_cmap('rainbow')

# plot line per line for each age group
for ag in range(len(ages)):
    color = gradient(1 / len(ages) * ag)
    plt.plot(df.iloc[:, ag], "-", label=ag, linewidth=0.75, color=color)

# plt.plot(df)

# add grid
plt.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.1)

# every second month on x-axis
plt.xticks(all_dates[1::2], rotation=90)

# add legend
plt.legend(ages, loc='upper right')

# label axes
plt.ylabel('Sterbefälle pro Monat')

plt.show()
