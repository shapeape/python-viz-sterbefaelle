#!/usr/bin/python3

"""
Generate a line diagram to show daily deaths in germany, comparing the years 2016-2020
"""

import matplotlib.pyplot as plt
from openpyxl import load_workbook

file = r'data/sonderauswertung-sterbefaelle.xlsx'

# data_only : read values instead of formulas
wb = load_workbook(file, data_only=True)
sheet = wb['D_2016_2020_Tage']

dates = []
deaths2020 = []
deaths2019 = []
deaths2018 = []
deaths2017 = []
deaths2016 = []
for col in range(2, 368):
    day = sheet.cell(column=col, row=9).value
    # we skip the 29.02. for each year
    if day == '29.02.':
        continue
    dates.append(day)
    deaths2020.append(sheet.cell(column=col, row=10).value)
    deaths2019.append(sheet.cell(column=col, row=11).value)
    deaths2018.append(sheet.cell(column=col, row=12).value)
    deaths2017.append(sheet.cell(column=col, row=13).value)
    deaths2016.append(sheet.cell(column=col, row=14).value)

plt.plot(dates, deaths2016, "--c", label="2016", linewidth=0.5)
plt.plot(dates, deaths2017, "--m", label="2017", linewidth=0.5)
plt.plot(dates, deaths2018, "--y", label="2018", linewidth=0.5)
plt.plot(dates, deaths2019, "--b", label="2019", linewidth=0.5)
plt.plot(dates, deaths2020, "-r", label="2020", linewidth=1)

# ticks for x-axis
plt.xticks(['01.01.', '01.02.', '01.03.', '01.04.', '01.05.', '01.06.', '01.07.', '01.08.', '01.09.', '01.10.',
            '01.11.', '01.12.'])

# print min and max values of both axes
print(plt.axis())
axes = plt.gca()
# y-axis starts at zero
axes.set_ylim(0.0, 4050.0)

# add grid
plt.grid(color='b', alpha=0.5, linestyle='dashed', linewidth=0.1)

# add legend
plt.legend(loc='upper right')

# label axes
plt.xlabel('Datum')
plt.ylabel('Sterbefälle pro Tag')

plt.show()
