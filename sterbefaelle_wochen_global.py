# compare mortality in different countries over time
# data from The Human Mortality Database (https://www.mortality.org/)


import pandas as pd
from bokeh.palettes import Dark2
from bokeh.plotting import figure, show

p = figure(plot_width=1200, plot_height=500, x_axis_type="datetime")

country_filter = ['DEUTNP', 'SVK', 'ITA', 'FRATNP', 'USA', 'RUS', 'ESP', 'KOR']
years = [2016, 2017, 2018, 2019, 2020]

# TODO: use me
# better to read names for some countries
country_rename = {'DEUTNP': 'DE', 'FRATNP': 'FRA'}

df = pd.read_csv('data/stmf.csv', comment='#')

# only both sexes
df = df.iloc[2::3, :]

# only country code, year, week, total death rate
df = df.iloc[:, [0, 1, 2, 15]]
# print(df.head())

# use only years 2016 mor recent
df = df.loc[df.Year > 2015, :]

# build datetime array as index
dates = []
year_count = 0
for year in years:
    for week in range(1, 53):
        dates.append(pd.to_datetime(year, format='%Y') + pd.to_timedelta(str(week * 7) + ' days'))

# loop throught countries and assign colors
for country, color in zip(country_filter, Dark2.get(len(country_filter))):
    country_df = pd.DataFrame()
    for year in years:
        # create masks
        mask_country1 = (df['CountryCode'] == country) & (df['Year'] == year)
        # use masks
        country_df_data = df.loc[mask_country1]
        # get RTotal values as pd series
        values = country_df_data['RTotal']
        # concat values; append them
        country_df = pd.concat([country_df, values], axis=0, ignore_index=True)

    # set dates as index, use only as much, as there is data
    country_df.index = dates[:len(country_df)]
    # name column
    country_df.columns = [country]
    # draw the line
    p.line(country_df.index, country_df[country], line_width=1, color=color, alpha=0.8,
           muted_color=color, muted_alpha=0.2, legend_label=country)

p.legend.location = "top_left"
p.legend.click_policy = "mute"

p.yaxis.axis_label = "total death rate"

p.xaxis.minor_tick_in = 12

p.xaxis.minor_tick_line_width = 5

show(p)
