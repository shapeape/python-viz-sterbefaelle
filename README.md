# python-viz-sterbefaelle

Visualize deaths in Germany during the years 2016-2020.
Done out of curiosity and to practice.


# Sources
Data is taken from Statistisches Bundesamt:  
https://www.destatis.de/DE/Themen/Gesellschaft-Umwelt/Bevoelkerung/Sterbefaelle-Lebenserwartung/Tabellen/sonderauswertung-sterbefaelle.html?nn=209016

# Results
## Daily Deaths
Comparing the years 2016-2020.
![daily deaths](results/sterbefaelle_taeglich.png "Title Text")

## Monthly Deaths
Analyzing how deaths change over time in different age groups.
![monthly deaths by age group](results/sterbefaelle_monate_ag.png "Title Text")